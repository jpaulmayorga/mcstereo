
import { Icon, Tag } from 'antd'

// routes
import { Link } from '../routes'

// Moment js
import moment from 'moment'

export const NoticeItem = ({ options, type }) => {
    moment.locale('es')
    return (
        <Link route={`/n/${options.slug}`}>
            <a className="boxNew">
                <Tag className="date"><Icon type="clock-circle" /> {moment(options.createdAt).fromNow()}</Tag>
                <Tag className="count"><Icon type="eye" /> { options.count }</Tag>
                <div className={`cover ${type}`}>
                   <img src={options.cover.url} />
                </div>
                <div className="title">
                    <h2>{options.title}</h2>
                    <p>{ options.notice }</p>
                </div>
            </a>
        </Link>
    )
}