import React, { useEffect, useState } from 'react'

import { Card, Col, Row} from 'antd'
const { Meta } = Card

import {Link} from '../routes'

// api
import { apiUrl } from '../api'

const MoreNews = () => {
    const [ notices, setNotices ] = useState(null)

    useEffect(() => {
        const getLatestNews =  async () => {
            const req = await fetch(`${apiUrl}/news?_limit=3&_sort=id:desc`)
            return await req.json()
        }
        getLatestNews().then(response => {
            setNotices(response)
        })
    }, [setNotices])
    return (
        <div className="container">
            <h2>Otras noticias</h2>
            <div className="bar bottom"></div>
            <Row className="moreNews">
                {
                    notices &&
                    notices.map(notice => (
                        <Link route={`/n/${notice.slug}`}>
                            <a>
                                <Col span={8}>
                                    <Card
                                        hoverable
                                        cover={<img alt="example" src={notice.cover.url} />}
                                    >
                                        <Meta title={notice.title} />
                                    </Card>
                                </Col>
                            </a>
                        </Link>
                    ))
                }
            </Row>
        </div>
    )
}

export default MoreNews