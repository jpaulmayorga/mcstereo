

const Wall = ({ blurred, children }) => (
    <div className="wall">
        <div className="blurred">
          <div className="middle">
            <img src={blurred} alt="blurred"/>
          </div>
        </div>
        <div className="content container mini">
            { children }
        </div>
    </div>
)

export default Wall