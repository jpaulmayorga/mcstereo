import { useEffect } from 'react'


// head
import Head from 'next/head'
//components
import Header from '../components/Header'

// styles
import '../assets/App.less'

const Layout = ({ children }) => {

    return (
        <>
            <Head>
                <title>Mi Ciudad Stereo</title>
                <meta name="description" content="Mi ciudad Stereo una radio posi" />

                <meta name="viewport" content="initial-scale=1.0, width=device-width" />

                {/* <meta property="og:url"         content="http://mcstereo.com/noticia/96" />
                <meta property="og:type"        content="website" />
                <meta property="og:title"       content="SENTENCIADO A 12 MESES" />
                <meta property="og:description" content="El 15 de agosto a las 15 horas aproximadamente tras denuncia de moradores se ejecutó un operativo a" />
                <meta property="og:image"       content="http://mcstereo.com/uploads/mini_1566943370.jpeg" /> */}

                <link rel="icon" href="/img/logo-solo.png" type="image/png" />
            </Head>
            <Header />
            { children }
            <div className="radio">
                <iframe src="https://myradiostream.com/embed/free.php?s=Miciudadestereo&amp;btnstyle=default&amp;preview=true" name="Gj76k9c7mYBa" frameBorder="0" scrolling="no" ></iframe>
            </div>
        </>
    )
}


export default Layout