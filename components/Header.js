import React from 'react'

import { Router, Link } from '../routes'

import { Row, Col, Menu, Affix } from 'antd'


import '../assets/Header.less'


const Header = () => {

    const goTo = (to) => {
        Router.pushRoute(to)
    }

    return (
        <Affix>
            <header>
                <Row className="container">
                    <Link route={`/`}>
                        <a>
                            <img src="/img/logo-solo.png" className="logo" />
                        </a>
                    </Link>
                    <Link route={`/`}>
                        <a>
                            <img src="/img/logo-letter.png" className="logo letters" />
                        </a>
                    </Link>
                    <Col span={24} className="textAlignRight">
                        <Menu mode="horizontal">
                            <Menu.Item key="home" onClick={() => goTo('/')}>
                                    Inicio
                            </Menu.Item>
                            <Menu.Item key="news" onClick={() => goTo('/')}>
                                Noticias
                            </Menu.Item>
                            <Menu.Item key="contact" onClick={() => goTo('/')}>
                                Contacto
                            </Menu.Item>
                            <Menu.Item key="trinchera" onClick={() => goTo('/')}>
                                Trinchera
                            </Menu.Item>
                            <Menu.Item key="publicidad" onClick={() => goTo('/')}>
                                Publicidad
                            </Menu.Item>
                        </Menu>
                    </Col>
                </Row>
            </header>
        </Affix>
    )
}

export default Header