import React, { useCallback, useState } from 'react'
import ImageZoom from "react-medium-image-zoom"

const Image = ({ src }) => {

    return (
        <div className="Image">
            <ImageZoom
                image={{
                    src: src,
                    alt: "Wall Image",
                    className: "img"
                }}
                zoomImage={{
                    src: src,
                    alt: "Wall Image",
                    className: "img--zoomed"
                }}
            />
        </div>
    )
    }
export default Image