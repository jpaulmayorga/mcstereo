const next = require('next')
const express = require('express')
const routes = require('./routes')

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })

const handler = routes.getRequestHandler(app)

app.prepare().then(() => {
    express().use(handler).listen(3000)
    console.log('App is running on por 3000')
})
