

import Layout from '../components/Layout'

const Error = ({ statusCode }) => {
    return (
        <Layout>
            <div style={{ marginTop: '5em', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <h1>
                    {
                        statusCode
                        ? `An error ${statusCode} occurred on server`
                        : 'An error occurred on client'
                    }
                </h1>
            </div>
        </Layout>
    )
}
Error.getInitialProps = ({ res, err }) => {
    const statusCode = res ? res.statusCode : err ? err.statusCode : 404
    return { statusCode }
}
export default Error