import React from 'react'

import { Row, Col, Carousel, Icon } from 'antd'

// fetch from server
import fetch from 'isomorphic-unfetch'

// styles
import '../assets/App.less'

// components
import Layout from '../components/Layout'
import { NoticeItem } from '../components/NoticeItem'

// api
import { apiUrl, domainUrl } from '../api'

// routes
import { Link } from '../routes'

// Moment js
import moment from 'moment'

const Home = ({
  news, walls, sponsors
}) => {
  console.log(news)
  return (
    <Layout>
      <div className="carousel">
        <Carousel autoplay>
          {
            walls.length > 0 &&
            walls.map(wall => (
              <div key={wall.id} className="sliderWall">
                <img src={wall.cover.url} alt="Wall" />
              </div>
            ))
          }
        </Carousel>
        <svg xmlns="http://www.w3.org/2000/svg" height="100" viewBox="0 0 1347.514 86">
          <path d="M0,45S359.78,90.13,663.8,77c208.433-9,438.108-47,683.714-63v86H0Z" />
        </svg>
      </div>
      {/* <div className="getSponsor">
        <div className="container mini">
          <Row>
            <Col xs={24} md={14}>
              <h2>Tu Publicidad en Mi Ciudad Stereo</h2>
            </Col>
            <Col xs={24} md={10}>
              <p>asdkasdgaksdhaksjn</p>
            </Col>
          </Row>
        </div>
      </div> */}
      <div className="container home">
        <Row>
          <Col xs={24} md={6} className="sponsors">
            <h2>Publicidad</h2>
            {
              sponsors.map(sponsor =>
                <Row>
                  <Col span={24} key={sponsor.id}>
                    <img src={sponsor.cover.url} alt="Sponsor" />
                  </Col>
                </Row>
              )
            }
          </Col>
          <Col xs={24} md={18}>
            <h2>Últimas Noticias</h2>
            <div className="latestNews">
              <div className="line">
                <div className="half">
                  <NoticeItem options={news[0]} />
                </div>
                <div className="half line-half">
                  <div className="line">
                    <NoticeItem options={news[1]} />
                  </div>
                  <div className="line">
                    <NoticeItem options={news[2]} />
                  </div>
                </div>
              </div>
              <div className="line">
                <div className="half">
                  <div className="line">
                    <div className="half">
                      <NoticeItem options={news[3]} type="vertical"/>
                    </div>
                    <div className="half">
                      <NoticeItem options={news[4]} type="vertical" />
                    </div>
                  </div>
                </div>
                <div className="half line-half">
                  <div className="line">
                    <NoticeItem options={news[5]} />
                  </div>
                  <div className="line">
                    <NoticeItem options={news[6]} />
                  </div>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </div>
      <Row className="more">
        <Col xs={24} md={24}>
          <div className="boxNew bottom">
            <div className="cover">
              <img src='https://mcstereo.s3.us-east-2.amazonaws.com/4fb78ab5585c48709f640cbff7f0a1c6.jpg' />
            </div>
            <div className="container">
              <div className="text">
                <h2>Tu Publicidad en Mi Ciudad Stereo</h2>
                <h3>
                  Contáctanos por Whatsapp:{` `}
                  <a href="https://api.whatsapp.com/send?phone=593999722498&text=Quiero%20anunciar%20en%20Mi%20Ciudad%20Stereo">0999722498</a>
                </h3>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </Layout>
  )
}
Home.getInitialProps = async () => {

  const reqNews = await fetch(`${apiUrl}/news?_limit=7&_sort=createdAt:DESC`)
  const reqWalls = await fetch(`${apiUrl}/walls`)
  const reqSponsors = await fetch(`${apiUrl}/sponsors`)

  const news = await reqNews.json()
  const walls = await reqWalls.json()
  const sponsors = await reqSponsors.json()

  return { news, walls, sponsors }
}
export default Home
