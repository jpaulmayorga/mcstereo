const routes = require('next-routes')

module.exports = routes()
    .add('index')
    .add('notFound', '/404')
    .add('notice', '/n/:slug')