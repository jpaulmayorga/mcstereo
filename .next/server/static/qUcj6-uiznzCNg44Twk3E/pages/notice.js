module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "/NE2":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/antd/lib/tooltip/style/index.js
var style = __webpack_require__("93XW");

// EXTERNAL MODULE: external "antd/lib/tooltip"
var tooltip_ = __webpack_require__("z6+L");
var tooltip_default = /*#__PURE__*/__webpack_require__.n(tooltip_);

// EXTERNAL MODULE: ./node_modules/antd/lib/affix/style/index.js
var affix_style = __webpack_require__("bUaZ");

// EXTERNAL MODULE: external "antd/lib/affix"
var affix_ = __webpack_require__("s9eH");
var affix_default = /*#__PURE__*/__webpack_require__.n(affix_);

// EXTERNAL MODULE: ./node_modules/antd/lib/page-header/style/index.js
var page_header_style = __webpack_require__("1/37");

// EXTERNAL MODULE: external "antd/lib/page-header"
var page_header_ = __webpack_require__("p9i8");
var page_header_default = /*#__PURE__*/__webpack_require__.n(page_header_);

// EXTERNAL MODULE: ./node_modules/antd/lib/tag/style/index.js
var tag_style = __webpack_require__("PAYn");

// EXTERNAL MODULE: external "antd/lib/tag"
var tag_ = __webpack_require__("P7Vo");
var tag_default = /*#__PURE__*/__webpack_require__.n(tag_);

// EXTERNAL MODULE: ./node_modules/antd/lib/icon/style/index.js
var icon_style = __webpack_require__("FGdI");

// EXTERNAL MODULE: external "antd/lib/icon"
var icon_ = __webpack_require__("BWRB");
var icon_default = /*#__PURE__*/__webpack_require__.n(icon_);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/parse-int.js
var parse_int = __webpack_require__("6BQ9");
var parse_int_default = /*#__PURE__*/__webpack_require__.n(parse_int);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/json/stringify.js
var stringify = __webpack_require__("9Jkg");
var stringify_default = /*#__PURE__*/__webpack_require__.n(stringify);

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__("xnum");
var head_default = /*#__PURE__*/__webpack_require__.n(head_);

// EXTERNAL MODULE: external "isomorphic-unfetch"
var external_isomorphic_unfetch_ = __webpack_require__("0bYB");
var external_isomorphic_unfetch_default = /*#__PURE__*/__webpack_require__.n(external_isomorphic_unfetch_);

// EXTERNAL MODULE: ./pages/_error.js
var _error = __webpack_require__("Y0NT");

// EXTERNAL MODULE: ./components/Layout.js + 1 modules
var Layout = __webpack_require__("5Yp1");

// CONCATENATED MODULE: ./components/Wall.js

var __jsx = external_react_default.a.createElement;

const Wall = ({
  blurred,
  children
}) => __jsx("div", {
  className: "wall"
}, __jsx("div", {
  className: "blurred"
}, __jsx("div", {
  className: "middle"
}, __jsx("img", {
  src: blurred,
  alt: "blurred"
}))), __jsx("div", {
  className: "content container mini"
}, children));

/* harmony default export */ var components_Wall = (Wall);
// EXTERNAL MODULE: external "react-medium-image-zoom"
var external_react_medium_image_zoom_ = __webpack_require__("SooC");
var external_react_medium_image_zoom_default = /*#__PURE__*/__webpack_require__.n(external_react_medium_image_zoom_);

// CONCATENATED MODULE: ./components/Image.js
var Image_jsx = external_react_default.a.createElement;



const Image = ({
  src
}) => {
  return Image_jsx("div", {
    className: "Image"
  }, Image_jsx(external_react_medium_image_zoom_default.a, {
    image: {
      src: src,
      alt: "Wall Image",
      className: "img"
    },
    zoomImage: {
      src: src,
      alt: "Wall Image",
      className: "img--zoomed"
    }
  }));
};

/* harmony default export */ var components_Image = (Image);
// EXTERNAL MODULE: ./node_modules/antd/lib/row/style/index.js
var row_style = __webpack_require__("hr7U");

// EXTERNAL MODULE: external "antd/lib/row"
var row_ = __webpack_require__("tI3Q");
var row_default = /*#__PURE__*/__webpack_require__.n(row_);

// EXTERNAL MODULE: ./node_modules/antd/lib/col/style/index.js
var col_style = __webpack_require__("fv9D");

// EXTERNAL MODULE: external "antd/lib/col"
var col_ = __webpack_require__("vsU0");
var col_default = /*#__PURE__*/__webpack_require__.n(col_);

// EXTERNAL MODULE: ./node_modules/antd/lib/card/style/index.js
var card_style = __webpack_require__("mN36");

// EXTERNAL MODULE: external "antd/lib/card"
var card_ = __webpack_require__("5rRV");
var card_default = /*#__PURE__*/__webpack_require__.n(card_);

// EXTERNAL MODULE: ./routes/index.js
var routes = __webpack_require__("lKNH");

// EXTERNAL MODULE: ./api/index.js
var api = __webpack_require__("bMwp");

// CONCATENATED MODULE: ./components/MoreNews.js






var MoreNews_jsx = external_react_default.a.createElement;

const {
  Meta
} = card_default.a;
 // api



const MoreNews = () => {
  const {
    0: notices,
    1: setNotices
  } = Object(external_react_["useState"])(null);
  Object(external_react_["useEffect"])(() => {
    const getLatestNews = async () => {
      const req = await fetch(`${api["a" /* apiUrl */]}/news?_limit=3&_sort=id:desc`);
      return await req.json();
    };

    getLatestNews().then(response => {
      setNotices(response);
    });
  }, [setNotices]);
  return MoreNews_jsx("div", {
    className: "container"
  }, MoreNews_jsx("h2", null, "Otras noticias"), MoreNews_jsx("div", {
    className: "bar bottom"
  }), MoreNews_jsx(row_default.a, {
    className: "moreNews"
  }, notices && notices.map(notice => MoreNews_jsx(routes["Link"], {
    route: `/n/${notice.slug}`
  }, MoreNews_jsx("a", null, MoreNews_jsx(col_default.a, {
    span: 8
  }, MoreNews_jsx(card_default.a, {
    hoverable: true,
    cover: MoreNews_jsx("img", {
      alt: "example",
      src: notice.cover.url
    })
  }, MoreNews_jsx(Meta, {
    title: notice.title
  }))))))));
};

/* harmony default export */ var components_MoreNews = (MoreNews);
// EXTERNAL MODULE: external "react-markdown"
var external_react_markdown_ = __webpack_require__("id0+");
var external_react_markdown_default = /*#__PURE__*/__webpack_require__.n(external_react_markdown_);

// EXTERNAL MODULE: external "disqus-react"
var external_disqus_react_ = __webpack_require__("1Yx3");
var external_disqus_react_default = /*#__PURE__*/__webpack_require__.n(external_disqus_react_);

// EXTERNAL MODULE: external "moment"
var external_moment_ = __webpack_require__("wy2R");
var external_moment_default = /*#__PURE__*/__webpack_require__.n(external_moment_);

// CONCATENATED MODULE: ./pages/notice.js












var notice_jsx = external_react_default.a.createElement;
 // head

 // fetch from server

 // next error

 //ant design




 // api

 // markdown

 // disqus

 // Moment js

 //Links



const Notice = ({
  errorCode,
  data
}) => {
  const {
    0: sponsors,
    1: setSponsors
  } = Object(external_react_["useState"])([]);

  if (errorCode || !data) {
    return notice_jsx(_error["default"], {
      statusCode: errorCode
    });
  }

  const disqusShortname = 'mi-ciudad-stereo';
  const disqusConfig = {
    url: `${api["b" /* domainUrl */]}/n/${data.slug}`,
    identifier: data.id,
    title: data.title
  };
  external_moment_default.a.locale('es');
  Object(external_react_["useEffect"])(() => {
    const updateCount = async data => {
      const req = await external_isomorphic_unfetch_default()(`${api["a" /* apiUrl */]}/news/${data.id}`, {
        method: 'PUT',
        body: stringify_default()(data.data),
        headers: {
          'Content-Type': 'application/json'
        }
      });
      await req.json();
    };

    const getSponsors = async () => {
      const req = await external_isomorphic_unfetch_default()(`${api["a" /* apiUrl */]}/sponsors`);
      return await req.json();
    };

    getSponsors().then(response => setSponsors(response));
    updateCount({
      id: data.id,
      data: {
        count: data.count === null ? 1 : parse_int_default()(data.count) + 1
      }
    });
  }, []);
  console.log(sponsors);
  return notice_jsx(Layout["a" /* default */], null, notice_jsx(head_default.a, null, notice_jsx("title", null, "Mi Ciudad Stereo | ", data.title), notice_jsx("meta", {
    name: "description",
    content: data.notice.substring(0, 200)
  }), notice_jsx("meta", {
    name: "viewport",
    content: "initial-scale=1.0, width=device-width"
  }), notice_jsx("meta", {
    property: "og:url",
    content: `https://mcstereo.com/n/${data.slug}`
  }), notice_jsx("meta", {
    property: "og:type",
    content: "article"
  }), notice_jsx("meta", {
    property: "og:title",
    content: data.title
  }), notice_jsx("meta", {
    property: "og:description",
    content: data.notice.replace('*', '').substring(0, 200)
  }), notice_jsx("meta", {
    property: "og:image",
    content: data.cover.url
  }), notice_jsx("link", {
    rel: "icon",
    href: "/img/logo-solo.png",
    type: "image/png"
  })), notice_jsx(components_Wall, {
    blurred: data.cover && `${data.cover.url}`
  }, notice_jsx("h1", null, data.title), notice_jsx("div", {
    className: "details"
  }, notice_jsx(tag_default.a, {
    color: "#333"
  }, notice_jsx(icon_default.a, {
    type: "clock-circle"
  }), " ", external_moment_default()(data.createdAt).fromNow()), notice_jsx(tag_default.a, {
    color: "#333"
  }, notice_jsx(icon_default.a, {
    type: "eye"
  }), " ", data.count === null ? 1 : data.count))), notice_jsx(affix_default.a, {
    offsetTop: 50,
    className: "headerFixed"
  }, notice_jsx("div", {
    className: "container"
  }, notice_jsx(page_header_default.a, {
    onBack: () => null,
    title: data.title,
    subTitle: external_moment_default()(data.createdAt).fromNow()
  }))), notice_jsx("article", null, data.cover && notice_jsx(components_Image, {
    src: `${data.cover.url}`
  }), notice_jsx(affix_default.a, {
    offsetTop: 150,
    className: "sharing"
  }, notice_jsx("div", null, notice_jsx("h3", null, "Comparte"), notice_jsx("p", null, "Queremos llegar mas lejos gracias a ti"), notice_jsx("div", {
    className: "icons"
  }, notice_jsx(tooltip_default.a, {
    title: "Compartir en Facebook",
    placement: "bottom"
  }, notice_jsx(routes["Link"], {
    to: `https://www.facebook.com/sharer/sharer.php?u=https://mcstereo.com/n/${data.slug}`
  }, notice_jsx("a", {
    target: "_blank"
  }, notice_jsx("div", {
    className: "iconSharing facebook"
  }, notice_jsx("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    fill: "#fff",
    width: "24",
    height: "24",
    viewBox: "0 0 24 24"
  }, notice_jsx("path", {
    d: "M9 8h-3v4h3v12h5v-12h3.642l.358-4h-4v-1.667c0-.955.192-1.333 1.115-1.333h2.885v-5h-3.808c-3.596 0-5.192 1.583-5.192 4.615v3.385z"
  })))))), notice_jsx(tooltip_default.a, {
    title: "Compartir por Whatsapp",
    placement: "bottom"
  }, notice_jsx(routes["Link"], {
    to: `https://wa.me/?text=https://mcstereo.com/n/${data.slug}`
  }, notice_jsx("a", {
    target: "_blank"
  }, notice_jsx("div", {
    className: "iconSharing whatsapp"
  }, notice_jsx("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    fill: "#fff",
    width: "24",
    height: "24",
    viewBox: "0 0 24 24"
  }, notice_jsx("path", {
    d: "M.057 24l1.687-6.163c-1.041-1.804-1.588-3.849-1.587-5.946.003-6.556 5.338-11.891 11.893-11.891 3.181.001 6.167 1.24 8.413 3.488 2.245 2.248 3.481 5.236 3.48 8.414-.003 6.557-5.338 11.892-11.893 11.892-1.99-.001-3.951-.5-5.688-1.448l-6.305 1.654zm6.597-3.807c1.676.995 3.276 1.591 5.392 1.592 5.448 0 9.886-4.434 9.889-9.885.002-5.462-4.415-9.89-9.881-9.892-5.452 0-9.887 4.434-9.889 9.884-.001 2.225.651 3.891 1.746 5.634l-.999 3.648 3.742-.981zm11.387-5.464c-.074-.124-.272-.198-.57-.347-.297-.149-1.758-.868-2.031-.967-.272-.099-.47-.149-.669.149-.198.297-.768.967-.941 1.165-.173.198-.347.223-.644.074-.297-.149-1.255-.462-2.39-1.475-.883-.788-1.48-1.761-1.653-2.059-.173-.297-.018-.458.13-.606.134-.133.297-.347.446-.521.151-.172.2-.296.3-.495.099-.198.05-.372-.025-.521-.075-.148-.669-1.611-.916-2.206-.242-.579-.487-.501-.669-.51l-.57-.01c-.198 0-.52.074-.792.372s-1.04 1.016-1.04 2.479 1.065 2.876 1.213 3.074c.149.198 2.095 3.2 5.076 4.487.709.306 1.263.489 1.694.626.712.226 1.36.194 1.872.118.571-.085 1.758-.719 2.006-1.413.248-.695.248-1.29.173-1.414z"
  })))))), notice_jsx(tooltip_default.a, {
    title: "Compartir por Twitter",
    placement: "bottom"
  }, notice_jsx(routes["Link"], {
    to: `https://twitter.com/share?text=Mirenlo&url=https://mcstereo.com/n/${data.slug}`
  }, notice_jsx("a", {
    target: "_blank"
  }, notice_jsx("div", {
    className: "iconSharing twitter"
  }, notice_jsx("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    fill: "#fff",
    width: "24",
    height: "24",
    viewBox: "0 0 24 24"
  }, notice_jsx("path", {
    d: "M24 4.557c-.883.392-1.832.656-2.828.775 1.017-.609 1.798-1.574 2.165-2.724-.951.564-2.005.974-3.127 1.195-.897-.957-2.178-1.555-3.594-1.555-3.179 0-5.515 2.966-4.797 6.045-4.091-.205-7.719-2.165-10.148-5.144-1.29 2.213-.669 5.108 1.523 6.574-.806-.026-1.566-.247-2.229-.616-.054 2.281 1.581 4.415 3.949 4.89-.693.188-1.452.232-2.224.084.626 1.956 2.444 3.379 4.6 3.419-2.07 1.623-4.678 2.348-7.29 2.04 2.179 1.397 4.768 2.212 7.548 2.212 9.142 0 14.307-7.721 13.995-14.646.962-.695 1.797-1.562 2.457-2.549z"
  }))))))))), notice_jsx("div", {
    className: "content"
  }, notice_jsx(external_react_markdown_default.a, {
    source: data.notice
  }), notice_jsx("h2", null, "Comentarios "), notice_jsx(external_disqus_react_default.a.DiscussionEmbed, {
    shortname: disqusShortname,
    config: disqusConfig
  }))), notice_jsx("section", {
    className: "withBackground withPaddingTop withPaddingBottom"
  }, notice_jsx(components_MoreNews, null)));
};

Notice.getInitialProps = async ({
  query
}) => {
  const {
    slug
  } = query;
  const req = await external_isomorphic_unfetch_default()(`${api["a" /* apiUrl */]}/news?slug=${slug}`);
  const errorCode = req.status > 200 ? req.status : false;
  const json = await req.json();
  return {
    errorCode,
    data: json[0]
  };
};

/* harmony default export */ var pages_notice = __webpack_exports__["default"] = (Notice);

/***/ }),

/***/ "0bYB":
/***/ (function(module, exports) {

module.exports = require("isomorphic-unfetch");

/***/ }),

/***/ "1/37":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("yCCu");

__webpack_require__("JT+3");

__webpack_require__("W93S");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "1Yx3":
/***/ (function(module, exports) {

module.exports = require("disqus-react");

/***/ }),

/***/ "1yXF":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("dnqb");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("/NE2");


/***/ }),

/***/ "5Yp1":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__("xnum");
var head_default = /*#__PURE__*/__webpack_require__.n(head_);

// EXTERNAL MODULE: ./node_modules/antd/lib/affix/style/index.js
var style = __webpack_require__("bUaZ");

// EXTERNAL MODULE: external "antd/lib/affix"
var affix_ = __webpack_require__("s9eH");
var affix_default = /*#__PURE__*/__webpack_require__.n(affix_);

// EXTERNAL MODULE: ./node_modules/antd/lib/row/style/index.js
var row_style = __webpack_require__("hr7U");

// EXTERNAL MODULE: external "antd/lib/row"
var row_ = __webpack_require__("tI3Q");
var row_default = /*#__PURE__*/__webpack_require__.n(row_);

// EXTERNAL MODULE: ./node_modules/antd/lib/col/style/index.js
var col_style = __webpack_require__("fv9D");

// EXTERNAL MODULE: external "antd/lib/col"
var col_ = __webpack_require__("vsU0");
var col_default = /*#__PURE__*/__webpack_require__.n(col_);

// EXTERNAL MODULE: ./node_modules/antd/lib/menu/style/index.js
var menu_style = __webpack_require__("PFYH");

// EXTERNAL MODULE: external "antd/lib/menu"
var menu_ = __webpack_require__("a5Fm");
var menu_default = /*#__PURE__*/__webpack_require__.n(menu_);

// EXTERNAL MODULE: ./routes/index.js
var routes = __webpack_require__("lKNH");

// EXTERNAL MODULE: ./assets/Header.less
var Header = __webpack_require__("yGLi");

// CONCATENATED MODULE: ./components/Header.js








var __jsx = external_react_default.a.createElement;




const Header_Header = () => {
  const goTo = to => {
    routes["Router"].pushRoute(to);
  };

  return __jsx(affix_default.a, null, __jsx("header", null, __jsx(row_default.a, {
    className: "container"
  }, __jsx(routes["Link"], {
    route: `/`
  }, __jsx("a", null, __jsx("img", {
    src: "/img/logo-solo.png",
    className: "logo"
  }))), __jsx(routes["Link"], {
    route: `/`
  }, __jsx("a", null, __jsx("img", {
    src: "/img/logo-letter.png",
    className: "logo letters"
  }))), __jsx(col_default.a, {
    span: 24,
    className: "textAlignRight"
  }, __jsx(menu_default.a, {
    mode: "horizontal"
  }, __jsx(menu_default.a.Item, {
    key: "home",
    onClick: () => goTo('/')
  }, "Inicio"), __jsx(menu_default.a.Item, {
    key: "news",
    onClick: () => goTo('/')
  }, "Noticias"), __jsx(menu_default.a.Item, {
    key: "contact",
    onClick: () => goTo('/')
  }, "Contacto"), __jsx(menu_default.a.Item, {
    key: "trinchera",
    onClick: () => goTo('/')
  }, "Trinchera"), __jsx(menu_default.a.Item, {
    key: "publicidad",
    onClick: () => goTo('/')
  }, "Publicidad"))))));
};

/* harmony default export */ var components_Header = (Header_Header);
// EXTERNAL MODULE: ./assets/App.less
var App = __webpack_require__("uVPO");

// CONCATENATED MODULE: ./components/Layout.js

var Layout_jsx = external_react_default.a.createElement;
 // head

 //components

 // styles



const Layout = ({
  children
}) => {
  return Layout_jsx(external_react_default.a.Fragment, null, Layout_jsx(head_default.a, null, Layout_jsx("title", null, "Mi Ciudad Stereo"), Layout_jsx("meta", {
    name: "description",
    content: "Mi ciudad Stereo una radio posi"
  }), Layout_jsx("meta", {
    name: "viewport",
    content: "initial-scale=1.0, width=device-width"
  }), Layout_jsx("link", {
    rel: "icon",
    href: "/img/logo-solo.png",
    type: "image/png"
  })), Layout_jsx(components_Header, null), children, Layout_jsx("div", {
    className: "radio"
  }, Layout_jsx("iframe", {
    src: "https://myradiostream.com/embed/free.php?s=Miciudadestereo&btnstyle=default&preview=true",
    name: "Gj76k9c7mYBa",
    frameBorder: "0",
    scrolling: "no"
  })));
};

/* harmony default export */ var components_Layout = __webpack_exports__["a"] = (Layout);

/***/ }),

/***/ "5rRV":
/***/ (function(module, exports) {

module.exports = require("antd/lib/card");

/***/ }),

/***/ "64Tc":
/***/ (function(module, exports) {



/***/ }),

/***/ "6BQ9":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("wa65");

/***/ }),

/***/ "7H9b":
/***/ (function(module, exports) {



/***/ }),

/***/ "90Kz":
/***/ (function(module, exports) {

module.exports = require("next-routes");

/***/ }),

/***/ "93XW":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("hEgN");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "9Jkg":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("fozc");

/***/ }),

/***/ "BWRB":
/***/ (function(module, exports) {

module.exports = require("antd/lib/icon");

/***/ }),

/***/ "FGdI":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("Ibq+");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "G851":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("GPnO");

__webpack_require__("MaXC");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "GPnO":
/***/ (function(module, exports) {



/***/ }),

/***/ "Ibq+":
/***/ (function(module, exports) {



/***/ }),

/***/ "JT+3":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("64Tc");

__webpack_require__("PFYH");

__webpack_require__("G851");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "MaXC":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("XJbt");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "P7Vo":
/***/ (function(module, exports) {

module.exports = require("antd/lib/tag");

/***/ }),

/***/ "PAYn":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("fcTV");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "PFYH":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("Svq7");

__webpack_require__("93XW");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "SooC":
/***/ (function(module, exports) {

module.exports = require("react-medium-image-zoom");

/***/ }),

/***/ "Svq7":
/***/ (function(module, exports) {



/***/ }),

/***/ "VEUW":
/***/ (function(module, exports) {



/***/ }),

/***/ "W93S":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("X6VN");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "X6VN":
/***/ (function(module, exports) {



/***/ }),

/***/ "XJbt":
/***/ (function(module, exports) {



/***/ }),

/***/ "Y0NT":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("5Yp1");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const Error = ({
  statusCode
}) => {
  return __jsx(_components_Layout__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"], null, __jsx("div", {
    style: {
      marginTop: '5em',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center'
    }
  }, __jsx("h1", null, statusCode ? `An error ${statusCode} occurred on server` : 'An error occurred on client')));
};

Error.getInitialProps = ({
  res,
  err
}) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return {
    statusCode
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Error);

/***/ }),

/***/ "Z8Mf":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("a6CB");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "a5Fm":
/***/ (function(module, exports) {

module.exports = require("antd/lib/menu");

/***/ }),

/***/ "a6CB":
/***/ (function(module, exports) {



/***/ }),

/***/ "bMwp":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return apiUrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return domainUrl; });
const apiUrl = 'https://api.mcstereo.com';
const domainUrl = 'https://mcstereo.com';

/***/ }),

/***/ "bUaZ":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("7H9b");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "dnqb":
/***/ (function(module, exports) {



/***/ }),

/***/ "fcTV":
/***/ (function(module, exports) {



/***/ }),

/***/ "fozc":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/json/stringify");

/***/ }),

/***/ "fv9D":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("1yXF");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "gnA7":
/***/ (function(module, exports) {



/***/ }),

/***/ "hEgN":
/***/ (function(module, exports) {



/***/ }),

/***/ "hr7U":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("1yXF");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "id0+":
/***/ (function(module, exports) {

module.exports = require("react-markdown");

/***/ }),

/***/ "lKNH":
/***/ (function(module, exports, __webpack_require__) {

const routes = __webpack_require__("90Kz");

module.exports = routes().add('index').add('notFound', '/404').add('notice', '/n/:slug');

/***/ }),

/***/ "mN36":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("gnA7");

__webpack_require__("Z8Mf");

__webpack_require__("hr7U");

__webpack_require__("fv9D");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "p9i8":
/***/ (function(module, exports) {

module.exports = require("antd/lib/page-header");

/***/ }),

/***/ "s9eH":
/***/ (function(module, exports) {

module.exports = require("antd/lib/affix");

/***/ }),

/***/ "tI3Q":
/***/ (function(module, exports) {

module.exports = require("antd/lib/row");

/***/ }),

/***/ "uVPO":
/***/ (function(module, exports) {



/***/ }),

/***/ "vsU0":
/***/ (function(module, exports) {

module.exports = require("antd/lib/col");

/***/ }),

/***/ "wa65":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/parse-int");

/***/ }),

/***/ "wy2R":
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),

/***/ "xnum":
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "yCCu":
/***/ (function(module, exports) {



/***/ }),

/***/ "yGLi":
/***/ (function(module, exports) {



/***/ }),

/***/ "z6+L":
/***/ (function(module, exports) {

module.exports = require("antd/lib/tooltip");

/***/ })

/******/ });