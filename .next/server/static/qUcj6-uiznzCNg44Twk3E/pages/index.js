module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "0bYB":
/***/ (function(module, exports) {

module.exports = require("isomorphic-unfetch");

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("RNiq");


/***/ }),

/***/ "1yXF":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("dnqb");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "3Qrg":
/***/ (function(module, exports) {



/***/ }),

/***/ "5Yp1":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__("xnum");
var head_default = /*#__PURE__*/__webpack_require__.n(head_);

// EXTERNAL MODULE: ./node_modules/antd/lib/affix/style/index.js
var style = __webpack_require__("bUaZ");

// EXTERNAL MODULE: external "antd/lib/affix"
var affix_ = __webpack_require__("s9eH");
var affix_default = /*#__PURE__*/__webpack_require__.n(affix_);

// EXTERNAL MODULE: ./node_modules/antd/lib/row/style/index.js
var row_style = __webpack_require__("hr7U");

// EXTERNAL MODULE: external "antd/lib/row"
var row_ = __webpack_require__("tI3Q");
var row_default = /*#__PURE__*/__webpack_require__.n(row_);

// EXTERNAL MODULE: ./node_modules/antd/lib/col/style/index.js
var col_style = __webpack_require__("fv9D");

// EXTERNAL MODULE: external "antd/lib/col"
var col_ = __webpack_require__("vsU0");
var col_default = /*#__PURE__*/__webpack_require__.n(col_);

// EXTERNAL MODULE: ./node_modules/antd/lib/menu/style/index.js
var menu_style = __webpack_require__("PFYH");

// EXTERNAL MODULE: external "antd/lib/menu"
var menu_ = __webpack_require__("a5Fm");
var menu_default = /*#__PURE__*/__webpack_require__.n(menu_);

// EXTERNAL MODULE: ./routes/index.js
var routes = __webpack_require__("lKNH");

// EXTERNAL MODULE: ./assets/Header.less
var Header = __webpack_require__("yGLi");

// CONCATENATED MODULE: ./components/Header.js








var __jsx = external_react_default.a.createElement;




const Header_Header = () => {
  const goTo = to => {
    routes["Router"].pushRoute(to);
  };

  return __jsx(affix_default.a, null, __jsx("header", null, __jsx(row_default.a, {
    className: "container"
  }, __jsx(routes["Link"], {
    route: `/`
  }, __jsx("a", null, __jsx("img", {
    src: "/img/logo-solo.png",
    className: "logo"
  }))), __jsx(routes["Link"], {
    route: `/`
  }, __jsx("a", null, __jsx("img", {
    src: "/img/logo-letter.png",
    className: "logo letters"
  }))), __jsx(col_default.a, {
    span: 24,
    className: "textAlignRight"
  }, __jsx(menu_default.a, {
    mode: "horizontal"
  }, __jsx(menu_default.a.Item, {
    key: "home",
    onClick: () => goTo('/')
  }, "Inicio"), __jsx(menu_default.a.Item, {
    key: "news",
    onClick: () => goTo('/')
  }, "Noticias"), __jsx(menu_default.a.Item, {
    key: "contact",
    onClick: () => goTo('/')
  }, "Contacto"), __jsx(menu_default.a.Item, {
    key: "trinchera",
    onClick: () => goTo('/')
  }, "Trinchera"), __jsx(menu_default.a.Item, {
    key: "publicidad",
    onClick: () => goTo('/')
  }, "Publicidad"))))));
};

/* harmony default export */ var components_Header = (Header_Header);
// EXTERNAL MODULE: ./assets/App.less
var App = __webpack_require__("uVPO");

// CONCATENATED MODULE: ./components/Layout.js

var Layout_jsx = external_react_default.a.createElement;
 // head

 //components

 // styles



const Layout = ({
  children
}) => {
  return Layout_jsx(external_react_default.a.Fragment, null, Layout_jsx(head_default.a, null, Layout_jsx("title", null, "Mi Ciudad Stereo"), Layout_jsx("meta", {
    name: "description",
    content: "Mi ciudad Stereo una radio posi"
  }), Layout_jsx("meta", {
    name: "viewport",
    content: "initial-scale=1.0, width=device-width"
  }), Layout_jsx("link", {
    rel: "icon",
    href: "/img/logo-solo.png",
    type: "image/png"
  })), Layout_jsx(components_Header, null), children, Layout_jsx("div", {
    className: "radio"
  }, Layout_jsx("iframe", {
    src: "https://myradiostream.com/embed/free.php?s=Miciudadestereo&btnstyle=default&preview=true",
    name: "Gj76k9c7mYBa",
    frameBorder: "0",
    scrolling: "no"
  })));
};

/* harmony default export */ var components_Layout = __webpack_exports__["a"] = (Layout);

/***/ }),

/***/ "7H9b":
/***/ (function(module, exports) {



/***/ }),

/***/ "8DDU":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("3Qrg");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "90Kz":
/***/ (function(module, exports) {

module.exports = require("next-routes");

/***/ }),

/***/ "93XW":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("hEgN");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "BWRB":
/***/ (function(module, exports) {

module.exports = require("antd/lib/icon");

/***/ }),

/***/ "FGdI":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("Ibq+");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "Ibq+":
/***/ (function(module, exports) {



/***/ }),

/***/ "P7Vo":
/***/ (function(module, exports) {

module.exports = require("antd/lib/tag");

/***/ }),

/***/ "PAYn":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("fcTV");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "PFYH":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("Svq7");

__webpack_require__("93XW");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "RNiq":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/antd/lib/row/style/index.js
var style = __webpack_require__("hr7U");

// EXTERNAL MODULE: external "antd/lib/row"
var row_ = __webpack_require__("tI3Q");
var row_default = /*#__PURE__*/__webpack_require__.n(row_);

// EXTERNAL MODULE: ./node_modules/antd/lib/col/style/index.js
var col_style = __webpack_require__("fv9D");

// EXTERNAL MODULE: external "antd/lib/col"
var col_ = __webpack_require__("vsU0");
var col_default = /*#__PURE__*/__webpack_require__.n(col_);

// EXTERNAL MODULE: ./node_modules/antd/lib/carousel/style/index.js
var carousel_style = __webpack_require__("8DDU");

// EXTERNAL MODULE: external "antd/lib/carousel"
var carousel_ = __webpack_require__("i0nf");
var carousel_default = /*#__PURE__*/__webpack_require__.n(carousel_);

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "isomorphic-unfetch"
var external_isomorphic_unfetch_ = __webpack_require__("0bYB");
var external_isomorphic_unfetch_default = /*#__PURE__*/__webpack_require__.n(external_isomorphic_unfetch_);

// EXTERNAL MODULE: ./assets/App.less
var App = __webpack_require__("uVPO");

// EXTERNAL MODULE: ./components/Layout.js + 1 modules
var Layout = __webpack_require__("5Yp1");

// EXTERNAL MODULE: ./node_modules/antd/lib/tag/style/index.js
var tag_style = __webpack_require__("PAYn");

// EXTERNAL MODULE: external "antd/lib/tag"
var tag_ = __webpack_require__("P7Vo");
var tag_default = /*#__PURE__*/__webpack_require__.n(tag_);

// EXTERNAL MODULE: ./node_modules/antd/lib/icon/style/index.js
var icon_style = __webpack_require__("FGdI");

// EXTERNAL MODULE: external "antd/lib/icon"
var icon_ = __webpack_require__("BWRB");
var icon_default = /*#__PURE__*/__webpack_require__.n(icon_);

// EXTERNAL MODULE: ./routes/index.js
var routes = __webpack_require__("lKNH");

// EXTERNAL MODULE: external "moment"
var external_moment_ = __webpack_require__("wy2R");
var external_moment_default = /*#__PURE__*/__webpack_require__.n(external_moment_);

// CONCATENATED MODULE: ./components/NoticeItem.js





var __jsx = external_react_default.a.createElement;
 // Moment js


const NoticeItem = ({
  options,
  type
}) => {
  external_moment_default.a.locale('es');
  return __jsx(routes["Link"], {
    route: `/n/${options.slug}`
  }, __jsx("a", {
    className: "boxNew"
  }, __jsx(tag_default.a, {
    className: "date"
  }, __jsx(icon_default.a, {
    type: "clock-circle"
  }), " ", external_moment_default()(options.createdAt).fromNow()), __jsx(tag_default.a, {
    className: "count"
  }, __jsx(icon_default.a, {
    type: "eye"
  }), " ", options.count), __jsx("div", {
    className: `cover ${type}`
  }, __jsx("img", {
    src: options.cover.url
  })), __jsx("div", {
    className: "title"
  }, __jsx("h2", null, options.title), __jsx("p", null, options.notice))));
};
// EXTERNAL MODULE: ./api/index.js
var api = __webpack_require__("bMwp");

// CONCATENATED MODULE: ./pages/index.js






var pages_jsx = external_react_default.a.createElement;

 // styles

 // components


 // api

 // routes

 // Moment js



const Home = ({
  news,
  walls,
  sponsors
}) => {
  console.log(news);
  return pages_jsx(Layout["a" /* default */], null, pages_jsx("div", {
    className: "carousel"
  }, pages_jsx(carousel_default.a, {
    autoplay: true
  }, walls.length > 0 && walls.map(wall => pages_jsx("div", {
    key: wall.id,
    className: "sliderWall"
  }, pages_jsx("img", {
    src: wall.cover.url,
    alt: "Wall"
  })))), pages_jsx("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    height: "100",
    viewBox: "0 0 1347.514 86"
  }, pages_jsx("path", {
    d: "M0,45S359.78,90.13,663.8,77c208.433-9,438.108-47,683.714-63v86H0Z"
  }))), pages_jsx("div", {
    className: "container home"
  }, pages_jsx(row_default.a, null, pages_jsx(col_default.a, {
    xs: 24,
    md: 6,
    className: "sponsors"
  }, pages_jsx("h2", null, "Publicidad"), sponsors.map(sponsor => pages_jsx(row_default.a, null, pages_jsx(col_default.a, {
    span: 24,
    key: sponsor.id
  }, pages_jsx("img", {
    src: sponsor.cover.url,
    alt: "Sponsor"
  }))))), pages_jsx(col_default.a, {
    xs: 24,
    md: 18
  }, pages_jsx("h2", null, "\xDAltimas Noticias"), pages_jsx("div", {
    className: "latestNews"
  }, pages_jsx("div", {
    className: "line"
  }, pages_jsx("div", {
    className: "half"
  }, pages_jsx(NoticeItem, {
    options: news[0]
  })), pages_jsx("div", {
    className: "half line-half"
  }, pages_jsx("div", {
    className: "line"
  }, pages_jsx(NoticeItem, {
    options: news[1]
  })), pages_jsx("div", {
    className: "line"
  }, pages_jsx(NoticeItem, {
    options: news[2]
  })))), pages_jsx("div", {
    className: "line"
  }, pages_jsx("div", {
    className: "half"
  }, pages_jsx("div", {
    className: "line"
  }, pages_jsx("div", {
    className: "half"
  }, pages_jsx(NoticeItem, {
    options: news[3],
    type: "vertical"
  })), pages_jsx("div", {
    className: "half"
  }, pages_jsx(NoticeItem, {
    options: news[4],
    type: "vertical"
  })))), pages_jsx("div", {
    className: "half line-half"
  }, pages_jsx("div", {
    className: "line"
  }, pages_jsx(NoticeItem, {
    options: news[5]
  })), pages_jsx("div", {
    className: "line"
  }, pages_jsx(NoticeItem, {
    options: news[6]
  })))))))), pages_jsx(row_default.a, {
    className: "more"
  }, pages_jsx(col_default.a, {
    xs: 24,
    md: 24
  }, pages_jsx("div", {
    className: "boxNew bottom"
  }, pages_jsx("div", {
    className: "cover"
  }, pages_jsx("img", {
    src: "https://mcstereo.s3.us-east-2.amazonaws.com/4fb78ab5585c48709f640cbff7f0a1c6.jpg"
  })), pages_jsx("div", {
    className: "container"
  }, pages_jsx("div", {
    className: "text"
  }, pages_jsx("h2", null, "Tu Publicidad en Mi Ciudad Stereo"), pages_jsx("h3", null, "Cont\xE1ctanos por Whatsapp:", ` `, pages_jsx("a", {
    href: "https://api.whatsapp.com/send?phone=593999722498&text=Quiero%20anunciar%20en%20Mi%20Ciudad%20Stereo"
  }, "0999722498"))))))));
};

Home.getInitialProps = async () => {
  const reqNews = await external_isomorphic_unfetch_default()(`${api["a" /* apiUrl */]}/news?_limit=7&_sort=createdAt:DESC`);
  const reqWalls = await external_isomorphic_unfetch_default()(`${api["a" /* apiUrl */]}/walls`);
  const reqSponsors = await external_isomorphic_unfetch_default()(`${api["a" /* apiUrl */]}/sponsors`);
  const news = await reqNews.json();
  const walls = await reqWalls.json();
  const sponsors = await reqSponsors.json();
  return {
    news,
    walls,
    sponsors
  };
};

/* harmony default export */ var pages = __webpack_exports__["default"] = (Home);

/***/ }),

/***/ "Svq7":
/***/ (function(module, exports) {



/***/ }),

/***/ "VEUW":
/***/ (function(module, exports) {



/***/ }),

/***/ "a5Fm":
/***/ (function(module, exports) {

module.exports = require("antd/lib/menu");

/***/ }),

/***/ "bMwp":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return apiUrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return domainUrl; });
const apiUrl = 'https://api.mcstereo.com';
const domainUrl = 'https://mcstereo.com';

/***/ }),

/***/ "bUaZ":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("7H9b");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "dnqb":
/***/ (function(module, exports) {



/***/ }),

/***/ "fcTV":
/***/ (function(module, exports) {



/***/ }),

/***/ "fv9D":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("1yXF");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "hEgN":
/***/ (function(module, exports) {



/***/ }),

/***/ "hr7U":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("1yXF");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "i0nf":
/***/ (function(module, exports) {

module.exports = require("antd/lib/carousel");

/***/ }),

/***/ "lKNH":
/***/ (function(module, exports, __webpack_require__) {

const routes = __webpack_require__("90Kz");

module.exports = routes().add('index').add('notFound', '/404').add('notice', '/n/:slug');

/***/ }),

/***/ "s9eH":
/***/ (function(module, exports) {

module.exports = require("antd/lib/affix");

/***/ }),

/***/ "tI3Q":
/***/ (function(module, exports) {

module.exports = require("antd/lib/row");

/***/ }),

/***/ "uVPO":
/***/ (function(module, exports) {



/***/ }),

/***/ "vsU0":
/***/ (function(module, exports) {

module.exports = require("antd/lib/col");

/***/ }),

/***/ "wy2R":
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),

/***/ "xnum":
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "yGLi":
/***/ (function(module, exports) {



/***/ })

/******/ });