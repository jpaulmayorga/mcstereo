module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("Y0NT");


/***/ }),

/***/ "1yXF":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("dnqb");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "5Yp1":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__("xnum");
var head_default = /*#__PURE__*/__webpack_require__.n(head_);

// EXTERNAL MODULE: ./node_modules/antd/lib/affix/style/index.js
var style = __webpack_require__("bUaZ");

// EXTERNAL MODULE: external "antd/lib/affix"
var affix_ = __webpack_require__("s9eH");
var affix_default = /*#__PURE__*/__webpack_require__.n(affix_);

// EXTERNAL MODULE: ./node_modules/antd/lib/row/style/index.js
var row_style = __webpack_require__("hr7U");

// EXTERNAL MODULE: external "antd/lib/row"
var row_ = __webpack_require__("tI3Q");
var row_default = /*#__PURE__*/__webpack_require__.n(row_);

// EXTERNAL MODULE: ./node_modules/antd/lib/col/style/index.js
var col_style = __webpack_require__("fv9D");

// EXTERNAL MODULE: external "antd/lib/col"
var col_ = __webpack_require__("vsU0");
var col_default = /*#__PURE__*/__webpack_require__.n(col_);

// EXTERNAL MODULE: ./node_modules/antd/lib/menu/style/index.js
var menu_style = __webpack_require__("PFYH");

// EXTERNAL MODULE: external "antd/lib/menu"
var menu_ = __webpack_require__("a5Fm");
var menu_default = /*#__PURE__*/__webpack_require__.n(menu_);

// EXTERNAL MODULE: ./routes/index.js
var routes = __webpack_require__("lKNH");

// EXTERNAL MODULE: ./assets/Header.less
var Header = __webpack_require__("yGLi");

// CONCATENATED MODULE: ./components/Header.js








var __jsx = external_react_default.a.createElement;




const Header_Header = () => {
  const goTo = to => {
    routes["Router"].pushRoute(to);
  };

  return __jsx(affix_default.a, null, __jsx("header", null, __jsx(row_default.a, {
    className: "container"
  }, __jsx(routes["Link"], {
    route: `/`
  }, __jsx("a", null, __jsx("img", {
    src: "/img/logo-solo.png",
    className: "logo"
  }))), __jsx(routes["Link"], {
    route: `/`
  }, __jsx("a", null, __jsx("img", {
    src: "/img/logo-letter.png",
    className: "logo letters"
  }))), __jsx(col_default.a, {
    span: 24,
    className: "textAlignRight"
  }, __jsx(menu_default.a, {
    mode: "horizontal"
  }, __jsx(menu_default.a.Item, {
    key: "home",
    onClick: () => goTo('/')
  }, "Inicio"), __jsx(menu_default.a.Item, {
    key: "news",
    onClick: () => goTo('/')
  }, "Noticias"), __jsx(menu_default.a.Item, {
    key: "contact",
    onClick: () => goTo('/')
  }, "Contacto"), __jsx(menu_default.a.Item, {
    key: "trinchera",
    onClick: () => goTo('/')
  }, "Trinchera"), __jsx(menu_default.a.Item, {
    key: "publicidad",
    onClick: () => goTo('/')
  }, "Publicidad"))))));
};

/* harmony default export */ var components_Header = (Header_Header);
// EXTERNAL MODULE: ./assets/App.less
var App = __webpack_require__("uVPO");

// CONCATENATED MODULE: ./components/Layout.js

var Layout_jsx = external_react_default.a.createElement;
 // head

 //components

 // styles



const Layout = ({
  children
}) => {
  return Layout_jsx(external_react_default.a.Fragment, null, Layout_jsx(head_default.a, null, Layout_jsx("title", null, "Mi Ciudad Stereo"), Layout_jsx("meta", {
    name: "description",
    content: "Mi ciudad Stereo una radio posi"
  }), Layout_jsx("meta", {
    name: "viewport",
    content: "initial-scale=1.0, width=device-width"
  }), Layout_jsx("link", {
    rel: "icon",
    href: "/img/logo-solo.png",
    type: "image/png"
  })), Layout_jsx(components_Header, null), children, Layout_jsx("div", {
    className: "radio"
  }, Layout_jsx("iframe", {
    src: "https://myradiostream.com/embed/free.php?s=Miciudadestereo&btnstyle=default&preview=true",
    name: "Gj76k9c7mYBa",
    frameBorder: "0",
    scrolling: "no"
  })));
};

/* harmony default export */ var components_Layout = __webpack_exports__["a"] = (Layout);

/***/ }),

/***/ "7H9b":
/***/ (function(module, exports) {



/***/ }),

/***/ "90Kz":
/***/ (function(module, exports) {

module.exports = require("next-routes");

/***/ }),

/***/ "93XW":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("hEgN");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "PFYH":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("Svq7");

__webpack_require__("93XW");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "Svq7":
/***/ (function(module, exports) {



/***/ }),

/***/ "VEUW":
/***/ (function(module, exports) {



/***/ }),

/***/ "Y0NT":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("5Yp1");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const Error = ({
  statusCode
}) => {
  return __jsx(_components_Layout__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"], null, __jsx("div", {
    style: {
      marginTop: '5em',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center'
    }
  }, __jsx("h1", null, statusCode ? `An error ${statusCode} occurred on server` : 'An error occurred on client')));
};

Error.getInitialProps = ({
  res,
  err
}) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return {
    statusCode
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Error);

/***/ }),

/***/ "a5Fm":
/***/ (function(module, exports) {

module.exports = require("antd/lib/menu");

/***/ }),

/***/ "bUaZ":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("7H9b");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "dnqb":
/***/ (function(module, exports) {



/***/ }),

/***/ "fv9D":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("1yXF");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "hEgN":
/***/ (function(module, exports) {



/***/ }),

/***/ "hr7U":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__("VEUW");

__webpack_require__("1yXF");
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "lKNH":
/***/ (function(module, exports, __webpack_require__) {

const routes = __webpack_require__("90Kz");

module.exports = routes().add('index').add('notFound', '/404').add('notice', '/n/:slug');

/***/ }),

/***/ "s9eH":
/***/ (function(module, exports) {

module.exports = require("antd/lib/affix");

/***/ }),

/***/ "tI3Q":
/***/ (function(module, exports) {

module.exports = require("antd/lib/row");

/***/ }),

/***/ "uVPO":
/***/ (function(module, exports) {



/***/ }),

/***/ "vsU0":
/***/ (function(module, exports) {

module.exports = require("antd/lib/col");

/***/ }),

/***/ "xnum":
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "yGLi":
/***/ (function(module, exports) {



/***/ })

/******/ });